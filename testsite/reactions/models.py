from django.db import models
from django.contrib.auth.models import User
from episodes.models import *
import datetime


class SoftDeleteManager(models.Manager):
    ''' Use this manager to get objects that have a deleted field '''
    def get_query_set(self):
        return super(SoftDeleteManager, self).get_query_set().filter(deleted=False)
    def all_with_deleted(self):
        return super(SoftDeleteManager, self).get_query_set()
    def deleted_set(self):
        return super(SoftDeleteManager, self).get_query_set().filter(deleted=True)


class AbstractReaction(models.Model):
    user = models.ForeignKey(User)
    created_at = models.DateTimeField()
    objects = SoftDeleteManager()
    deleted_at = models.DateTimeField(blank=True, null=True)
    deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def delete(self, *args, **kwargs):

        self.deleted_at = datetime.datetime.now()
        self.deleted=True
        self.save()

    def save(self, *args, **kwargs):
        if self.deleted is False:
            self.deleted_at = None
        else:
            if not self.created_at:
                self.deleted=False
            else:
                self.deleted_at = datetime.datetime.now()
        super(AbstractReaction, self).save(*args, **kwargs)


class ImageReaction(AbstractReaction):
    image = models.ImageField()
    episode = models.ForeignKey("episodes.Episode", blank=True, null=True, related_name="image_reactions")

    def __unicode__(self):
        return "Image reaction " + str(self.pk) + "- episode " + str(self.episode.episode_number)


class TweetReaction(AbstractReaction):
    text = models.CharField(max_length=150)
    episode = models.ForeignKey("episodes.Episode", blank=True, null=True, related_name="tweet_reactions")


    def __unicode__(self):
        return "Tweet reaction " + str(self.pk) + "- episode " + str(self.episode.episode_number)