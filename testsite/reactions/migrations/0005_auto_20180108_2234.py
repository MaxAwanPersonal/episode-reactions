# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reactions', '0004_auto_20180108_2220'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='imagereaction',
            name='deleted',
        ),
        migrations.RemoveField(
            model_name='tweetreaction',
            name='deleted',
        ),
        migrations.AlterField(
            model_name='imagereaction',
            name='episode',
            field=models.ForeignKey(related_name='image_reactions', blank=True, to='episodes.Episode', null=True),
        ),
        migrations.AlterField(
            model_name='tweetreaction',
            name='episode',
            field=models.ForeignKey(related_name='tweet_reactions', blank=True, to='episodes.Episode', null=True),
        ),
    ]
