# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('episodes', '0001_initial'),
        ('reactions', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='imagereaction',
            name='episode',
            field=models.ForeignKey(related_name='image_reactions', blank=True, to='episodes.Episode', null=True),
        ),
        migrations.AddField(
            model_name='tweetreaction',
            name='episode',
            field=models.ForeignKey(related_name='tweet_reactions', blank=True, to='episodes.Episode', null=True),
        ),
    ]
