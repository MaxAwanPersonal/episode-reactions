# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reactions', '0005_auto_20180108_2234'),
    ]

    operations = [
        migrations.AddField(
            model_name='imagereaction',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='imagereaction',
            name='deleted_at',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='tweetreaction',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='tweetreaction',
            name='deleted_at',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
