# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reactions', '0002_auto_20180108_2155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imagereaction',
            name='episode',
            field=models.ForeignKey(related_name='image_reactions', to='episodes.Episode'),
        ),
        migrations.AlterField(
            model_name='tweetreaction',
            name='episode',
            field=models.ForeignKey(related_name='tweet_reactions', to='episodes.Episode'),
        ),
    ]
