# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reactions', '0003_auto_20180108_2202'),
    ]

    operations = [
        migrations.AddField(
            model_name='imagereaction',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='tweetreaction',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
    ]
