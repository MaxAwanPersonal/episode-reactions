from django.contrib import admin

from .models import ImageReaction, TweetReaction


class ReactionAdmin(admin.ModelAdmin):

    def get_actions(self, request):
        #Disable delete
        actions = super(ReactionAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        return False


class ImageReactionAdmin(ReactionAdmin):
    model = ImageReaction
    list_display = ('id', '__unicode__', 'deleted',)
    list_filter = ('deleted',)

    def queryset(self, request):
        """ Returns a QuerySet of all model instances that can be edited by the
        admin site. This is used by changelist_view. """
        # Default: qs = self.model._default_manager.get_query_set()
        qs = self.model._default_manager.all_with_deleted()
        # TODO: this should be handled by some parameter to the ChangeList.
        ordering = self.ordering or () # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs


class TweetReactionAdmin(ReactionAdmin):
    model = TweetReaction
    list_display = ('id', '__unicode__', 'deleted',)
    list_filter = ('deleted',)

    def queryset(self, request):
        """ Returns a QuerySet of all model instances that can be edited by the
        admin site. This is used by changelist_view. """
        # Default: qs = self.model._default_manager.get_query_set()
        qs = self.model._default_manager.all_with_deleted()
        # TODO: this should be handled by some parameter to the ChangeList.
        ordering = self.ordering or () # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs



admin.site.register(ImageReaction, ImageReactionAdmin)
admin.site.register(TweetReaction, TweetReactionAdmin)

