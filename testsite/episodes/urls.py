__author__ = 'max'
from django.conf.urls import patterns, url

from episodes import views


urlpatterns = patterns('',
    url(r'^:(?P<episode_number>\d+)$', views.episode_feed, name='episode_feed'))