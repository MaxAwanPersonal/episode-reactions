from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from episodes.models import Episode
from reactions.models import ImageReaction, TweetReaction
# Create your views here.


def episode_feed(request, episode_number=None):
    if not episode_number:
        return render(request, "permission_denied.html", {})
    try:
        episode = Episode.objects.get(episode_number=episode_number)
    except Episode.DoesNotExist:
        return render(request, "permission_denied.html", {})
    image_reactions = ImageReaction.objects.filter(deleted=False, episode=episode).order_by('-created_at')
    tweet_reactions = TweetReaction.objects.filter(deleted=False, episode=episode).order_by('-created_at')
    reactions = list(image_reactions) + list(tweet_reactions)
    sorted_reactions = sorted(reactions, key=lambda x: x.created_at, reverse=True)
    context = {
            "episode": episode,
            "reactions": sorted_reactions,
        }
    return render(request, "episode_feed.html", context)