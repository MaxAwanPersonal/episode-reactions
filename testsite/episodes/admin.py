from django.contrib import admin

from .models import Episode
from reactions.models import ImageReaction, TweetReaction


class ImageReactionInline(admin.TabularInline):
    model = ImageReaction

    def has_delete_permission(self, request, obj=None):
        return False

class TweetReactionInline(admin.TabularInline):
    model = TweetReaction

    def has_delete_permission(self, request, obj=None):
        return False

class EpisodeAdmin(admin.ModelAdmin):
    model = Episode
    inlines = [
        ImageReactionInline,
        TweetReactionInline,
    ]
    def queryset(self, request):
        """ Returns a QuerySet of all model instances that can be edited by the
        admin site. This is used by changelist_view. """
        # Default: qs = self.model._default_manager.get_query_set()
        qs = self.model._default_manager.all_with_deleted()
        # TODO: this should be handled by some parameter to the ChangeList.
        ordering = self.ordering or () # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

admin.site.register(Episode, EpisodeAdmin)
